import java.util.Scanner;

public class Main {

    static String[] board;
    static String turn = "X";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        board = new String[9];
        String win = "continue";

        System.out.println("Welcome to TicTacToe. \nGoodLuck");
        populateEmptyBoard();
        printBoard();
        System.out.println("X goes first. Chose a spot: ");


        while (win.equals("continue")) {
            int inputNumber = scanner.nextInt();

            if (board[inputNumber - 1].equals("X") || board[inputNumber - 1].equals("O")) {
                System.out.println("Spot taken. Chose onther one");
                inputNumber = scanner.nextInt();
            }

            board[inputNumber - 1] = turn;

            printBoard();
            win = winner();

            if (win.equals("DRAW")){
                System.out.println("It's a draw! Thanks for playing.");
                break;
            } else if (win.equals("X") || win.equals("O")){
                System.out.println("Congratulations! " + win + "'s have won!");
                break;
            }

            if (turn.equals("X")) {
                turn = "O";
            } else {
                turn = "X";
            }
            System.out.println(turn + "'s turn. Enter a slot number to place " + turn + " in:");
        }
    }

    public static void populateEmptyBoard() {
        for (int i = 0; i < 9; i++) {
            board[i] = String.valueOf(i + 1);
        }
    }

    public static void printBoard() {
        System.out.println("/---|---|---\\");
        System.out.println("| " + board[0] + " " + "| " + board[1] + " " + "| " + board[2] + " |");
        System.out.println("|-----------|");
        System.out.println("| " + board[3] + " " + "| " + board[4] + " " + "| " + board[5] + " |");
        System.out.println("|-----------|");
        System.out.println("| " + board[6] + " " + "| " + board[7] + " " + "| " + board[8] + " |");
        System.out.println("\\---|---|---/");
    }

    public static String winner() {
        int count = 0;
        if ((board[0] + board[1] + board[2]).equals("XXX") || (board[0] + board[1] + board[2]).equals("OOO")) {
            return board[0];
        } else if ((board[3] + board[4] + board[5]).equals("XXX") || (board[3] + board[4] + board[5]).equals("OOO")) {
            return board[3];
        } else if ((board[6] + board[7] + board[8]).equals("XXX") || (board[6] + board[7] + board[8]).equals("OOO")) {
            return board[6];
        } else if ((board[0] + board[3] + board[6]).equals("XXX") || (board[0] + board[3] + board[6]).equals("OOO")) {
            return board[0];
        } else if ((board[1] + board[4] + board[7]).equals("XXX") || (board[1] + board[4] + board[7]).equals("OOO")) {
            return board[1];
        } else if ((board[2] + board[5] + board[8]).equals("XXX") || (board[2] + board[5] + board[8]).equals("OOO")) {
            return board[2];
        } else if ((board[0] + board[4] + board[8]).equals("XXX") || (board[0] + board[4] + board[8]).equals("OOO")) {
            return board[0];
        } else if ((board[2] + board[4] + board[6]).equals("XXX") || (board[2] + board[4] + board[6]).equals("OOO")) {
            return board[2];
        }
        for (int i = 0; i < 9; i++) {
            if (board[i].equals("X") || board[i].equals("O")){
                count++;
            }
        }
        if (count == 9) {
            return "DRAW";
        }
        return "continue";
    }
}
